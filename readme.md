# Installation
```shell
composer require gaslampmedia/document-template-converter --no-dev
```

# Usage
```php
use GaslampMedia\DocumentTemplateConverter\Word\WordTemplate

// initialization. currently only supports docx files
$template = new WordTemplate('/full/path/to/sample.docx');

// replace a single template variable
$template->replace('first_name', 'Johnny');

// replace multiple template variables
$template->replace([
    'first_name' => 'Johnny',
    'last_name' => 'Bravo',
]);

$template->saveAsPdf('/full/path/to/filename.pdf');
// or
$template->saveAsWord('/full/path/to/filename.docx');
```
**Important**
Once you save it, you won't be able to save it again due to cleanup that happens immediately after saving in PhpWord.

## Advanced Usage
In case you want to do more using the underlying `PhpOffice\PhpWord\TemplateProcessor`, you have direct access to it.
```php
use GaslampMedia\DocumentTemplateConverter\Word\WordTemplate
$template = new WordTemplate('/full/path/to/sample.docx');
$template->processor()->deleteBlock('block_name');
```