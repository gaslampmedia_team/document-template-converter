<?php

namespace GaslampMedia\DocumentTemplateConverter;

use GaslampMedia\DocumentTemplateConverter\Exceptions\FileDoesNotExistException;
use GaslampMedia\DocumentTemplateConverter\Exceptions\UnreadableFileException;
use GaslampMedia\DocumentTemplateConverter\Pdf\MpdfWriter;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\TemplateProcessor;
use Respect\Validation\Validator;

abstract class Template
{
    protected $processor;
    protected $allowedMimeTypes = [];

    /**
     * @param $filePath
     *
     * @throws \GaslampMedia\DocumentTemplateConverter\Exceptions\FileDoesNotExistException
     * @throws \GaslampMedia\DocumentTemplateConverter\Exceptions\UnreadableFileException
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     */
    public function __construct($filePath)
    {
        \PhpOffice\PhpWord\Settings::setPdfRendererPath(dirname(__DIR__) . '/vendor/mpdf/mpdf');
        \PhpOffice\PhpWord\Settings::setPdfRendererName('MPDF');
        \PhpOffice\PhpWord\Settings::setTempDir(sys_get_temp_dir());

        if ( ! file_exists($filePath)) {
            throw new FileDoesNotExistException("$filePath does not exist");
        }

        if ( ! is_readable($filePath)) {
            throw new UnreadableFileException("$filePath is not readable");
        }

        $validatedMimeTypes = array_filter($this->allowedMimeTypes, function ($mimeType) use ($filePath) {
            return Validator::mimetype($mimeType)->validate($filePath);
        });

        if ( ! $validatedMimeTypes) {
            throw new \InvalidArgumentException("$filePath is not one of the allowed mimetypes: " . implode(', ',
                    $this->allowedMimeTypes));
        }

        $this->processor = new TemplateProcessor($filePath);
    }

    public static function create(string $filePath)
    {
        return new static($filePath);
    }

    /**
     * @param string|string[] $keyOrArray
     * @param string          $value
     *
     * @return $this
     */
    public function replace($keyOrArray, $value = ''): self
    {
        if (is_array($keyOrArray)) {
            foreach ($keyOrArray as $key => $val) {
                $this->replace($key, $val);
            }
        } else {
            $this->processor->setValue($keyOrArray, $value);
        }

        return $this;
    }

    /**
     * @param string $filePath
     *
     * @throws \Mpdf\MpdfException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function saveAsPdf(string $filePath): void
    {
        $tempFilePath = $this->saveAsTemporaryWord();

        $pdfWriter = new MpdfWriter(IOFactory::load($tempFilePath));
//        $pdfWriter->setTempDir(sys_get_temp_dir());
//        $pdfTempFile = $this->createTempFileName();

        $pdfWriter->save($filePath);

        @unlink($tempFilePath);
    }

    /**
     * @param string $filePath
     *
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function saveAsWord(string $filePath): void
    {
        $this->processor->saveAs($filePath);
    }

    public function processor(): TemplateProcessor
    {
        return $this->processor;
    }

    protected function createTempFileName(): string
    {
        return tempnam(sys_get_temp_dir(), time());
    }

    /**
     * @return string
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    protected function saveAsTemporaryWord(): string
    {
        $filePath = $this->createTempFileName();
        $this->saveAsWord($filePath);

        return $filePath;
    }
}