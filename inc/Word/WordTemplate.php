<?php

namespace GaslampMedia\DocumentTemplateConverter\Word;

use GaslampMedia\DocumentTemplateConverter\Template;

class WordTemplate extends Template
{
    protected $allowedMimeTypes = [
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.documentapplication/vnd.openxmlformats-officedocument.wordprocessingml.document'
    ];
}