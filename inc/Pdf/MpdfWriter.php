<?php

namespace GaslampMedia\DocumentTemplateConverter\Pdf;

class MpdfWriter extends \PhpOffice\PhpWord\Writer\PDF\MPDF
{
    /**
     * @param null $filename
     *
     * @throws \Mpdf\MpdfException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function save($filename = null): void
    {
        $fileHandle = $this->prepareForSave($filename);

        //  Create PDF
        $pdf = new \Mpdf\Mpdf([
            'tempDir' => sys_get_temp_dir(),
        ]);

        //  PDF settings
        $orientation = 'PORTRAIT';
        $pdf->_setPageSize('A4', $orientation);
        $pdf->addPage($orientation);

        // Write document properties
        $phpWord = $this->getPhpWord();
        $docProps = $phpWord->getDocInfo();
        $pdf->setTitle($docProps->getTitle());
        $pdf->setAuthor($docProps->getCreator());
        $pdf->setSubject($docProps->getSubject());
        $pdf->setKeywords($docProps->getKeywords());
        $pdf->setCreator($docProps->getCreator());

        $pdf->writeHTML($this->getContent());

        //$pdf->SetProtection([
        //    //'copy',
        //    //'print',
        //    //'modify',
        //    //'annot-forms',
        //    //'fill-forms',
        //    //'extract',
        //    //'assemble',
        //    //'print-highres',
        //]);

        //  Write to file
        fwrite($fileHandle, $pdf->output($filename, 'S'));

        $this->restoreStateAfterSave($fileHandle);
    }
}