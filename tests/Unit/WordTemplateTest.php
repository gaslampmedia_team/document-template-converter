<?php

use GaslampMedia\DocumentTemplateConverter\Exceptions\FileDoesNotExistException;
use GaslampMedia\DocumentTemplateConverter\Exceptions\UnreadableFileException;
use GaslampMedia\DocumentTemplateConverter\Word\WordTemplate;
use org\bovigo\vfs\vfsStream;

it('throws a FileDoesNotExistException when file does not exist', function () {
    $root = vfsStream::setup();
    try {
        WordTemplate::create($root->url() . '/sample.docx');
    } catch (FileDoesNotExistException $exception) {
        $this->assertInstanceOf(FileDoesNotExistException::class, $exception);
    }
});

it('throws an UnreadableFileException when file is not readable', function () {
    $root = vfsStream::setup();
    vfsStream::newFile('sample.docx', 0400)
             ->at($root)
             ->setContent('sample content')
             ->chown(vfsStream::OWNER_ROOT);

    try {
        WordTemplate::create($root->url() . '/sample.docx');
    } catch (UnreadableFileException $exception) {
        $this->assertInstanceOf(UnreadableFileException::class, $exception);
    }
});

it('throws an InvalidArgumentException when the file is not a word doc', function() {
    $root = vfsStream::setup();
    vfsStream::newFile('sample.txt')
             ->at($root)
             ->setContent('sample content');

    try {
        WordTemplate::create($root->url().'/sample.txt');
    } catch (InvalidArgumentException $exception) {
        $this->assertInstanceOf(InvalidArgumentException::class, $exception);
    }
});

it('allows word docs', function() {
    $template = WordTemplate::create(dirname(__DIR__).'/resources/sample.docx');
    $this->assertInstanceOf(WordTemplate::class, $template);
});