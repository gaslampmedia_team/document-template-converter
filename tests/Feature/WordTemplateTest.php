<?php

use GaslampMedia\DocumentTemplateConverter\Word\WordTemplate;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use Smalot\PdfParser\Parser;

it('can save as a pdf', function(){
    $template = new WordTemplate(dirname(__DIR__).'/resources/sample.docx');
    $filename = dirname(__DIR__).'/resources/result.pdf';

    $template->saveAsPdf($filename);

    $this->assertFileExists($filename);
    // this will error out if pdf format is incorrect
    $this->assertNotEmpty((new Parser())->parseFile($filename)->getText());
});

it('can save as a docx', function(){
    $template = new WordTemplate(dirname(__DIR__).'/resources/sample.docx');
    $filePath = dirname(__DIR__).'/resources/result.docx';
    $template->saveAsWord($filePath);

    // this will error out if it's not a docx
    $this->assertInstanceOf(PhpWord::class, IOFactory::load($filePath));
    $this->assertFileExists($filePath);
});

it('can replace a single value', function() {
    $template = new WordTemplate(dirname(__DIR__).'/resources/sample.docx');
    $filename = dirname(__DIR__).'/resources/result.pdf';
    $parser = new Parser();

    $template->saveAsPdf($filename);
    $this->assertStringNotContainsString('Johnny', $parser->parseFile($filename)->getText());

    $newTemplate = new WordTemplate(dirname(__DIR__).'/resources/sample.docx');
    $newTemplate->replace('first_name', 'Johnny');
    $newTemplate->saveAsPdf($filename);

    $this->assertStringContainsString('Johnny', $parser->parseFile($filename)->getText());
});

it('can replace multiple values', function() {
    $template = new WordTemplate(dirname(__DIR__).'/resources/sample.docx');
    $filename = dirname(__DIR__).'/resources/result.pdf';
    $parser = new Parser();

    $template->replace([
        'first_name' => 'Johnny',
        'last_name' => 'Bravo'
    ]);
    $template->saveAsPdf($filename);

    $this->assertStringContainsString('Johnny', $parser->parseFile($filename)->getText());
    $this->assertStringContainsString('Bravo', $parser->parseFile($filename)->getText());
});

afterEach(function() {
   @unlink(dirname(__DIR__).'/resources/result.docx');
   @unlink(dirname(__DIR__).'/resources/result.pdf');
});